//
//  InvitedEventsTableViewController.swift
//  TalentApp
//
//  Created by Leon Fu on 11/10/15.
//  Copyright © 2015 Leon Fu. All rights reserved.
//

import UIKit

class TableSectionHeader: UITableViewHeaderFooterView {
    
    @IBOutlet weak var flipButton: UIButton!
}

class InvitedEventsTableViewController: PFQueryTableViewController {
    // MARK: Init
    
    convenience init(className: String?) {
        self.init(style: .Plain, className: className)
        
        title = "Events"
        pullToRefreshEnabled = true
        objectsPerPage = 20
        paginationEnabled = true
    }
    
    override func awakeFromNib() {
        self.parseClassName = "InvitedTalentForEvents"
        let nib = UINib(nibName: "TableSectionHeader", bundle: nil)
        tableView.registerNib(nib, forHeaderFooterViewReuseIdentifier: "TableSectionHeader")
    }
    
    // MARK: Data
    
    override func queryForTable() -> PFQuery {
        return super.queryForTable().whereKey("talentObject", equalTo:PFUser.currentUser()!)
    }
    
    // MARK: TableView
    override func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 140.0
    }
    
    override func tableView(tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
    }
    
    override func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        // Dequeue with the reuse identifier
        let cell = self.tableView.dequeueReusableHeaderFooterViewWithIdentifier("TableSectionHeader")
        //let header = cell as! TableSectionHeader
        cell?.translatesAutoresizingMaskIntoConstraints = true
        cell?.autoresizingMask = [.FlexibleWidth, .FlexibleHeight]

        return cell
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath, object: PFObject?) -> PFTableViewCell? {
        let cellIdentifier = "cell"
        
        var cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier) as? PFTableViewCell
        if cell == nil {
            cell = PFTableViewCell(style: .Subtitle, reuseIdentifier: cellIdentifier)
        }
        
        let eventObject = object!["eventObject"] as! PFObject
        eventObject.fetchIfNeededInBackgroundWithBlock { (event: PFObject?, error: NSError?) -> Void in
            cell?.textLabel?.text = event!["eventName"] as? String
            cell?.detailTextLabel?.text = "Status: " + (object!["status"] as! String)
        }

        return cell
    }
 
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self.performSegueWithIdentifier("showEventDetailViewController", sender: indexPath)
    }
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "showEventDetailViewController" {
            let indexPath = sender as! NSIndexPath
            let eventDetailVC = segue.destinationViewController as! EventDetailViewController
            eventDetailVC.invitedEvent = self.objectAtIndexPath(indexPath)
        }
    }

    @IBAction func flipButtonClicked(sender: UIButton) {
        if let storyboardName = NSBundle.mainBundle().infoDictionary?["UIMainStoryboardFile"] as? String {
            let storyboard = UIStoryboard.init(name: storyboardName, bundle: NSBundle.mainBundle())
            let viewProfileViewController = storyboard.instantiateViewControllerWithIdentifier("ViewProfileViewController")
            viewProfileViewController.modalTransitionStyle = .FlipHorizontal
            self.presentViewController(viewProfileViewController, animated: true, completion: { () -> Void in
                
            })
        }
    }
    
    @IBAction func exitDetail(segue: UIStoryboardSegue) {
        self.loadObjects()
    }

}
