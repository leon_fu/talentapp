//
//  EditProfileViewController.swift
//  TalentApp
//
//  Created by Leon Fu on 11/14/15.
//  Copyright © 2015 Leon Fu. All rights reserved.
//

import UIKit

class EditProfileViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var bioTextView: UITextView!
    @IBOutlet weak var ageTextField: UITextField!
    @IBOutlet weak var nameLabel: UILabel!

    @IBOutlet var profileImages: [UIImageView]!
    
    private var loadedTalent: PFObject!
    private var tappedImageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Let's load the data from Parse.
        let query = PFQuery(className:"Talents")
        query.whereKey("User", equalTo: PFUser.currentUser()!)
        query.getFirstObjectInBackgroundWithBlock { (talent: PFObject?, error: NSError?) -> Void in
            if (talent != nil) {
                self.nameLabel.text = (talent?["talentName"]) as? String
                self.bioTextView.text = (talent?["bio"]) as? String
                if let age = ((talent?["age"]) as? Int) {
                    self.ageTextField.text = String(age)
                }
                
                // Load Photos if we have them.
                if let photosArray = talent?.objectForKey("Photos") as? [PFFile] {
                    var photoIndex = 0
                    for photo in photosArray {
                        photo.getDataInBackgroundWithBlock({ (imageData: NSData?, error: NSError?) -> Void in
                            if error == nil {
                                if let imageData = imageData {
                                    self.profileImages[photoIndex++].image = UIImage(data: imageData)
                                }
                            }
                        })
                    }
                }
                self.loadedTalent = talent
            }
        }
        
        // Let's add a tap gesture recognizer to all the images
        for profileImage in self.profileImages {
            let tapGesture = UITapGestureRecognizer(target: self, action: "imageTapped:")
            profileImage.addGestureRecognizer(tapGesture)
        }
    }
    
    override func viewWillDisappear(animated: Bool) {
        if (self.loadedTalent != nil) {
            self.loadedTalent["age"] = Int(self.ageTextField.text!)
            self.loadedTalent["bio"] = self.bioTextView.text
            self.loadedTalent["talentName"] = self.nameLabel.text
            self.loadedTalent.saveInBackground()
        }
    }
    
    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
            return false
        }
        
        return true
    }
    
    func imageTapped(gestureRecognizer: UITapGestureRecognizer) {
        tappedImageView = gestureRecognizer.view as? UIImageView
        
        // Let's bring up the camera roll
        let imagePicker = UIImagePickerController.init()
        imagePicker.allowsEditing = false
        imagePicker.delegate = self
        imagePicker.allowsEditing = true
        
        let alertView = UIAlertController.init(title: nil, message: nil, preferredStyle: UIAlertControllerStyle.ActionSheet)
        
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera) {
            imagePicker.sourceType = UIImagePickerControllerSourceType.Camera
            let cameraControl = UIAlertAction.init(title: "Camera", style: UIAlertActionStyle.Default) { (alert: UIAlertAction!) -> Void in
                self.presentViewController(imagePicker, animated: true) { () -> Void in
                    
                }
            }
            
            alertView.addAction(cameraControl)
        }
        
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.PhotoLibrary) {
            let photoLibraryControl = UIAlertAction.init(title: "Photo Library", style: UIAlertActionStyle.Default) { (alert: UIAlertAction!) -> Void in
                imagePicker.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
                self.presentViewController(imagePicker, animated: true) { () -> Void in
                    
                }
            }
            
            alertView.addAction(photoLibraryControl)
        }
        
        let cancelAction = UIAlertAction.init(title: "Cancel", style: UIAlertActionStyle.Cancel) { (alert: UIAlertAction!) -> Void in
            
        }
        alertView.addAction(cancelAction)
        
        self.presentViewController(alertView, animated: true) { () -> Void in
            
        }
    }

    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        if let image = info[UIImagePickerControllerEditedImage] as? UIImage {
            // Process and save image.
            var minSizeWidth = image.size.width
            var minSizeHeight = image.size.height
            
            if (minSizeHeight > 1000 || minSizeHeight > 1000) {
                minSizeHeight /= 3
                minSizeWidth /= 3
            }
            
            let photoString = "Photos"
            let scaledImage = UIImage.scaleUIImageToSize(image, size: CGSizeMake(minSizeWidth, minSizeHeight))
            let imageFile = PFFile(name: photoString, data: UIImageJPEGRepresentation(scaledImage, 0.6)!)
            tappedImageView.image = scaledImage
            
            if let photosArray = self.loadedTalent?.objectForKey("Photos") as? [PFFile] {
                if photosArray.count < 6  {
                    self.loadedTalent.addObject(imageFile!, forKey: photoString)
                    self.loadedTalent.saveInBackground()
                }
            }
        }
        
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        // If they cancel, delete the image.
        let photoIndex = tappedImageView.tag
        if let photosArray = self.loadedTalent?.objectForKey("Photos") as? [PFFile] {
            self.loadedTalent.removeObject(photosArray[photoIndex], forKey: "Photos")
            tappedImageView.image = nil
            self.loadedTalent.saveInBackground()
        }
        
        self.dismissViewControllerAnimated(true, completion: nil)
            
    }
    
    @IBAction func deletePhotoClicked(sender: UIButton) {
        let photoIndex = sender.tag
        let imageView = self.profileImages[photoIndex]
        
        if let photosArray = self.loadedTalent?.objectForKey("Photos") as? [PFFile] {
            self.loadedTalent.removeObject(photosArray[photoIndex], forKey: "Photos")
            self.loadedTalent.saveInBackgroundWithBlock({ (succeeded: Bool, error: NSError?) -> Void in
                imageView.image = nil
            })
        }
        
    }
}
