//
//  WelcomeToViewController.swift
//  TalentApp
//
//  Created by Leon Fu on 2/13/16.
//  Copyright © 2016 Leon Fu. All rights reserved.
//

import UIKit

class WelcomeToViewController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.addGradientBackground()
    }
}
