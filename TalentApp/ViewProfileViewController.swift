//
//  ViewProfileViewController.swift
//  TalentApp
//
//  Created by Leon Fu on 11/14/15.
//  Copyright © 2015 Leon Fu. All rights reserved.
//

import UIKit

class ImageViewCollectionCell: UICollectionViewCell {
    @IBOutlet weak var imageView: UIImageView!
}

class ProfileImageTableViewCell: UITableViewCell {
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var pageControl: UIPageControl!
}

class ProfileInfoTableViewCell: UITableViewCell {
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var nextLabel: UILabel!
}

class AboutMeTableViewCell: UITableViewCell {
    @IBOutlet weak var aboutMeLabel: UILabel!
    @IBOutlet weak var aboutMeTextView: UITextView!
}

class ViewProfileViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var headerView: UIView!
    
    var profileImageViewTableCell: ProfileImageTableViewCell!
    private var loadedTalent: PFObject!
    var profileImages = [UIImage]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        // Let's load the data from Parse.
        let query = PFQuery(className:"Talents")
        query.whereKey("User", equalTo: PFUser.currentUser()!)
        query.getFirstObjectInBackgroundWithBlock { (talent: PFObject?, error: NSError?) -> Void in
            if (talent != nil) {
                self.loadedTalent = talent
                
                // Load Photos if we have them.
                if let photosArray = talent?.objectForKey("Photos") as? [PFFile] {
                    self.profileImages.removeAll()
                    for photo in photosArray {
                        photo.getDataInBackgroundWithBlock({ (imageData: NSData?, error: NSError?) -> Void in
                            if error == nil {
                                if let image = UIImage(data: imageData!) {
                                    self.profileImages.append(image)
                                    if self.profileImageViewTableCell != nil {
                                        self.profileImageViewTableCell.collectionView.reloadData()
                                    }
                                }
                            }
                        })
                    }
                }
                
                self.tableView.reloadData()
            }
        }
    }
    
    @IBAction func exitEditProfileViewController(segue: UIStoryboardSegue) {
        
    }
 
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var tableViewCell: UITableViewCell?
        
        if indexPath.row == 0 {
            if profileImageViewTableCell == nil {
                profileImageViewTableCell = tableView.dequeueReusableCellWithIdentifier("profileImageCell") as! ProfileImageTableViewCell
                profileImageViewTableCell.collectionView.pagingEnabled = true
            }
            
            tableViewCell = profileImageViewTableCell
        } else if indexPath.row == 1 {
            let profileInfoCell = tableView.dequeueReusableCellWithIdentifier("profileInfoCell") as! ProfileInfoTableViewCell
            profileInfoCell.nameLabel.text = (self.loadedTalent?["talentName"]) as? String
            profileInfoCell.cityLabel.text = (self.loadedTalent?["location"]) as? String
            tableViewCell = profileInfoCell
        } else if indexPath.row == 2 {
            let aboutMeCell = tableView.dequeueReusableCellWithIdentifier("aboutMeCell") as! AboutMeTableViewCell
            aboutMeCell.aboutMeTextView.text = (self.loadedTalent?["bio"]) as? String
            tableViewCell = aboutMeCell
        }
        
        return tableViewCell!
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        var rowHeight: CGFloat = 0.0
        
        if indexPath.row == 0 {
            rowHeight = self.view.bounds.width
        } else if indexPath.row == 1 {
            rowHeight = 100.0
        } else if indexPath.row == 2 {
            rowHeight = 190.0
        }

        return rowHeight
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let imageCount = self.profileImages.count
        profileImageViewTableCell.pageControl.numberOfPages = imageCount
        
        return imageCount
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        var collectionViewCell: ImageViewCollectionCell
        
        collectionViewCell = collectionView.dequeueReusableCellWithReuseIdentifier("imageViewCollectionCell", forIndexPath: indexPath) as! ImageViewCollectionCell
        
        // Load photos here.
        collectionViewCell.imageView.image = self.profileImages[indexPath.row]
        return collectionViewCell
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        let size = CGSize.init(width: collectionView.bounds.width, height: collectionView.bounds.width - 1)
        
        return size
    }
    
    func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
        let pageNumber = roundf(Float(scrollView.contentOffset.x / scrollView.frame.size.width))
        profileImageViewTableCell.pageControl.currentPage = Int(pageNumber)
    }

    @IBAction func flipButtonClicked(sender: UIButton) {
        self.modalTransitionStyle = .FlipHorizontal
        self.dismissViewControllerAnimated(true) { () -> Void in
            
        }
    }
}
