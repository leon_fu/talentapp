//
//  EventDetailViewController.swift
//  TalentApp
//
//  Created by Leon Fu on 11/10/15.
//  Copyright © 2015 Leon Fu. All rights reserved.
//

import UIKit

class EventDetailViewController: UIViewController {
    @IBOutlet weak var eventDetailsLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var actionButton: UIButton!
    
    var invitedEvent: PFObject!
    var eventObject: PFObject!
    var currentStatus: String? {
        didSet {
            // Set the action button depending on the current status
            invitedEvent["status"] = currentStatus // Update our status with the current status
            self.statusLabel.text = currentStatus
            self.actionButton.enabled = true
            
            // Now take action based on what the new status is.
            invitedEvent.saveInBackgroundWithBlock { (success: Bool, error: NSError?) -> Void in
                if (self.currentStatus == "Invited") {
                    self.actionButton.setTitle("Accept", forState: UIControlState.Normal)
                } else if (self.currentStatus == "Accepted") {
                    self.actionButton.setTitle("Check In", forState: UIControlState.Normal)
                } else if (self.currentStatus == "Checked In") {
                    self.actionButton.setTitle("Check Out", forState: UIControlState.Normal)
                } else if (self.currentStatus == "Verified") {
                    self.actionButton.enabled = false
                }
            }
        }
    }
    
    override func viewDidLoad() {
        if (invitedEvent != nil) {
            currentStatus = invitedEvent["status"] as? String
            self.statusLabel.text = currentStatus
            
            // Populate the event details screen
            eventObject = invitedEvent["eventObject"] as? PFObject
            eventObject.fetchIfNeededInBackgroundWithBlock({ (event: PFObject?, error: NSError?) -> Void in
                self.eventDetailsLabel.text = event?["eventName"] as? String
            })
        }
    }
    
    @IBAction func actionClicked(sender: UIButton) {
        if (currentStatus == "Invited") { // Invited->Accepted
            currentStatus = "Accepted"
        } else if (currentStatus == "Accepted") { //Accepted->Checked In
            currentStatus = "Checked In"
            self.performSegueWithIdentifier("showCheckedInViewController", sender: self)
        }
    }

    @IBAction func exitCheckedInDetail(segue: UIStoryboardSegue) {
        
    }

}
