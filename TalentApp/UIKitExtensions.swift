//
//  UIKitExtensions.swift
//  TalentApp
//
//  Created by Leon Fu on 2/23/16.
//  Copyright © 2016 Leon Fu. All rights reserved.
//

import UIKit

extension UIButton {
    private func imageWithColor(color: UIColor) -> UIImage {
        let rect = CGRectMake(0.0, 0.0, 1.0, 1.0)
        UIGraphicsBeginImageContext(rect.size)
        let context = UIGraphicsGetCurrentContext()
        
        CGContextSetFillColorWithColor(context, color.CGColor)
        CGContextFillRect(context, rect)
        
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return image
    }
    
    func setBackgroundColor(color: UIColor, forUIControlState state: UIControlState) {
        self.setBackgroundImage(imageWithColor(color), forState: state)
    }
}

extension UIViewController {
    func addGradientBackground() {
        // Create the gradient
        // 1
        self.view.backgroundColor = UIColor.blackColor()
        
        // 2
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = self.view.bounds
        gradientLayer.startPoint = CGPoint(x: 1, y: 0)
        gradientLayer.endPoint = CGPoint(x: 0, y: 1)
        
        // 3
        let color1 = UIColor(red: 178.0/255, green: 88.0/255, blue: 89.0/255, alpha: 1.0).CGColor as CGColorRef
        let color2 = UIColor.blackColor().CGColor as CGColorRef
        gradientLayer.colors = [color1, color2]
        
        // 4
        gradientLayer.locations = [0.0, 1.0]
        
        // 5
        self.view.layer.insertSublayer(gradientLayer, atIndex: 0)
    }
}

extension UIView {
    func addGradientBackground() {
        // Create the gradient
        // 1
        self.backgroundColor = UIColor.blackColor()
        
        // 2
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = self.bounds
//        gradientLayer.startPoint = CGPoint(x: 1, y: 0)
//        gradientLayer.endPoint = CGPoint(x: 0, y: 1)
        
        // 3
        let color1 = UIColor(red: 178.0/255, green: 88.0/255, blue: 89.0/255, alpha: 1.0).CGColor as CGColorRef
        let color2 = UIColor.blackColor().CGColor as CGColorRef
        gradientLayer.colors = [color1, color2]
        
        // 4
        gradientLayer.locations = [0.0, 1.0]
        
        // 5
        self.layer.insertSublayer(gradientLayer, atIndex: 0)
    }

}