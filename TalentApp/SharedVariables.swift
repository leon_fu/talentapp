//
//  SharedVariables.swift
//  Event Creator
//
//  Created by Leon Fu on 10/31/15.
//  Copyright © 2015 Leon Fu. All rights reserved.
//

import Foundation

class SharedVariables {
    var userLoggedIn: PFUser!
//    let ref = Firebase(url: "https://popping-heat-7411.firebaseio.com")
    
    private init () {
    }
    
    static let sharedInstance = SharedVariables()
}