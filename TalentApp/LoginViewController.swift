//
//  LoginViewController.swift
//  Event Creator
//
//  Created by Leon Fu on 10/30/15.
//  Copyright © 2015 Leon Fu. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController, UIScrollViewDelegate {
    @IBOutlet weak var fbLoginButton: UIButton!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var scrollView: UIScrollView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        fbLoginButton.layer.opaque = true
        fbLoginButton.layer.borderWidth = 3.0
        fbLoginButton.layer.cornerRadius = 3.0
        fbLoginButton.layer.borderColor = UIColor(red: 200.0/255, green: 170.0/255, blue: 114.0/255, alpha: 1.0).CGColor
        fbLoginButton.setBackgroundColor(UIColor(red: 200.0/255, green: 170.0/255, blue: 114.0/255, alpha: 1.0), forUIControlState: UIControlState.Highlighted)
        fbLoginButton.setBackgroundColor(UIColor.whiteColor(), forUIControlState: UIControlState.Normal)
        
        self.addGradientBackground()
    }
    
    // MARK: - FBSDKLoginButtonDelegate
    
    @IBAction func fbLoginButtonClicked(sender: UIButton) {
        PFFacebookUtils.logInInBackgroundWithReadPermissions(["public_profile", "user_about_me", "email", "user_friends", "user_photos", "user_location"]) {
            (user: PFUser?, error: NSError?) -> Void in
            
            SharedVariables.sharedInstance.userLoggedIn = user
            if user == nil {
                print("Uh oh. The user cancelled the Facebook login.")
            }
            
            self.performSegueWithIdentifier("ExitLogin", sender: self)
        }
    }
    
    func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
        let pageNumber = roundf(Float(scrollView.contentOffset.x / scrollView.frame.size.width))
        pageControl.currentPage = Int(pageNumber)
    }
}
