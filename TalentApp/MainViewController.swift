//
//  MainViewController.swift
//  TalentApp
//
//  Created by Leon Fu on 11/5/15.
//  Copyright © 2015 Leon Fu. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {
    var currentState = 0
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        let accessToken = FBSDKAccessToken.currentAccessToken()
        if (accessToken == nil) { // If we haven't logged in, segue to the login page.
            self.performSegueWithIdentifier("loginViewControllerSegue", sender: self)
        } else { // Login with the token if we haven't already.
            if SharedVariables.sharedInstance.userLoggedIn == nil {
                PFFacebookUtils.logInInBackgroundWithAccessToken(accessToken, block: { (user: PFUser?, error: NSError?) -> Void in
                    SharedVariables.sharedInstance.userLoggedIn = user
                    
                    if user == nil {
                        print("Uh oh. There was an error logging in.")
                    } else {
                        self.advanceState()
                    }
                })
            } else {
                self.advanceState()
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func loginDone(segue: UIStoryboardSegue) {
        if let fbProfile = SharedVariables.sharedInstance.userLoggedIn {
            // Verify we got everything
            print("User Id:" + fbProfile.username!)
        }
    }

    @IBAction func dismissManualCodeEntry(segue: UIStoryboardSegue) {
        if let fbProfile = SharedVariables.sharedInstance.userLoggedIn {
            // Verify we got everything
            print("User Id:" + fbProfile.username!)
        }
    }

    @IBAction func dismissWelcomeTo(segue: UIStoryboardSegue) {
        if let fbProfile = SharedVariables.sharedInstance.userLoggedIn {
            // Verify we got everything
            print("User Id:" + fbProfile.username!)
        }
    }

    func advanceState() {
        self.addTalent({ () -> Void in
            if self.currentState == 0 {
                self.performSegueWithIdentifier("showWelcomeToViewController", sender: self)
            } else if self.currentState == 1 {
                self.performSegueWithIdentifier("showInvitesViewController", sender: self)
                //self.performSegueWithIdentifier("showManualEntryViewController", sender: self)
            }
//            else if self.currentState == 2 {
//                self.performSegueWithIdentifier("showTabBarController", sender: self)
//            }
            
            self.currentState++
        })
    }
    
    func addTalent(completion: ()->Void) { // Add the logged in talent if it doesn't exist
        // First check if the talent already exists:
        let query = PFQuery(className:"Talents")
        query.whereKey("User", equalTo:PFUser.currentUser()!)
        query.getFirstObjectInBackgroundWithBlock({ (user:PFObject?, error:NSError?) -> Void in
            if user == nil { // There was no users, so we need to add us to Talents Table
                // Let's do a graph request.
                FBSDKGraphRequest.init(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email, bio, age_range, gender, location"]).startWithCompletionHandler({ (connection: FBSDKGraphRequestConnection!, result: AnyObject!, error: NSError!) -> Void in
                    if (error == nil) {
                        if let dict = result as? NSDictionary {
                            print(dict)
                            let talentQueryCount = PFQuery(className:"Talents")
                            talentQueryCount.countObjectsInBackgroundWithBlock({ (count: Int32, error:NSError?) -> Void in
                                let newUser = PFObject(className: "Talents")
                                newUser["User"] = PFUser.currentUser()
                                if let nameString = dict.objectForKey("name") as? String {
                                    newUser["talentName"] = nameString
                                }
                                if let firstName = dict.objectForKey("first_name") as? String {
                                    newUser["firstName"] = firstName
                                }
                                if let lastName = dict.objectForKey("last_name") as? String {
                                    newUser["lastName"] = lastName
                                }
                                if let location = dict.objectForKey("location")?.objectForKey("name") as? String {
                                    newUser["location"] = location
                                }
                                newUser["age"] = 0
                                if let bioText = dict.objectForKey("bio") as? String {
                                    newUser["bio"] = bioText
                                }
                                newUser["index"] = NSNumber.init(int: count)
                                
                                newUser.saveInBackground()
                                completion()
                            })
                        }
                    } else {
                        // Handle graph error here.
                    }
                })
            } else {
                completion()
            }
        })
    }
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
//    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
//        // Get the new view controller using segue.destinationViewController.
//        // Pass the selected object to the new view controller.
//        if segue.identifier == "showTabBarController" {
//        }
//    }

}

